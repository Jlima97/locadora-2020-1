package br.ucsal.bes20201.testequalidade.locadora;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import br.ucsal.bes20201.testequalidade.locadora.business.LocacaoBO;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;
import builder.VeiculoBuilder;

public class LocacaoBOIntegradoTest {

	/**
	 * Testar o cálculo do valor de locação por
	 *  3 dias para 2 veículos com 1 ano de
	 * fabricaçao e 3 veículos com 8 anos de fabricação.
	 */
	/**esperado 2115*/
	@Test 
	public void testarCalculoValorTotalLocacao5Veiculos3Dias() {
	Veiculo veiculo1 = VeiculoBuilder.umVeiculoBuilder().build();
	Veiculo veiculo2 = VeiculoBuilder.umVeiculoBuilder().build();
	Veiculo veiculo3 = VeiculoBuilder.umVeiculoBuilder().comAno(2012).build();
	Veiculo veiculo4 = VeiculoBuilder.umVeiculoBuilder().comAno(2012).build();
	Veiculo veiculo5 = VeiculoBuilder.umVeiculoBuilder().comAno(2012).build();
	
	 List<Veiculo> veiculos = new ArrayList<Veiculo>();
	 veiculos.add(veiculo1);
	 veiculos.add(veiculo2);
	 veiculos.add(veiculo3);
	 veiculos.add(veiculo4);
	 veiculos.add(veiculo5);
	 	 
	LocacaoBO locbo = new LocacaoBO();
	Double result =  locbo.calcularValorTotalLocacao(veiculos, 3);
	Double resultE = (double) 2115;
	System.out.println(result);
	assertEquals(resultE, result);
	
	
	
	
	
	}

}
